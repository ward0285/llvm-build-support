#!/bin/sh

set -e

echo "*********************************************"
pwd
echo "*********************************************"

mkdir -p build ; cd build


cmake \
  -DBUILD_SHARED_LIBS=On \
  -DLLVM_TARGETS_TO_BUILD=host \
  -DLLVM_INSTALL_UTILS=ON \
  -DCMAKE_BUILD_TYPE=Release \
  -DCMAKE_C_COMPILER=gcc \
  -DCMAKE_CXX_COMPILER=g++ \
  -DLLVM_ENABLE_PROJECTS="clang;mlir;flang;compiler-rt;openmp" \
  -DCOMPILER_RT_BUILD_SANITIZERS=OFF \
  -DCMAKE_CXX_STANDARD=17 \
  -DFLANG_ENABLE_WERROR=OFF \
  -DLLVM_ENABLE_ASSERTIONS=ON \
  -DLLVM_ENABLE_TERMINFO=OFF \
  -DCMAKE_CXX_LINK_FLAGS="-Wl,-rpath=$LD_LIBRARY_PATH -Wl,-disable-new-dtags" \
  --trace \
  ../llvm

  
  echo make -j2
  
